# MATLAB-IVC
MATLAB Image Video Convert (MATLAB-IVC) provide
a set of scripts for conversion
between video files and images.

Scripts is developed to be used in MATLAB.

Please read demo scripts before using these code.

Examples of converting images to a video file
is listed in **f2v_demo.m**.

Examples of converting a video file to images
is listed in **v2f_demo.m**.

## Examples
Images convert to a video file

``` MATLAB
opt = ivc.opt();
opt.F_Path      = 'select';
opt.FPS         = 20;
opt.F_Format    = '%04d.jpg';
ivc.f2v(opt);
clear opt;
```

A video file convert to images

``` MATLAB
opt = ivc.opt();
opt.V_Name    = 'input';
opt.F_Format  = '%04d.jpg';
ivc.v2f(opt);
clear opt;
```

## Config
opt have several property:

opt.V_Name : input or output video name,
when set as 'input',
script will show a dialog for selecting video file

opt.F_Format : input or output image file format,
when set as 'select',
script will show a dialog for selecting the folder

opt.F_Path : folder path of image files

opt.FPS : frames per second

opt.F_Start : start frame number

opt.F_End : end frame number

## Notice

+ If setting changes,
you need to clear the environment of MATLAB.
+ You should at least provide one parameter
for each option.
+ If you want to use scripts outside of this folder,
you can follow the instruction in **startup.m**.

## License

MATLAB-IVC is released under the [MIT license](https://bitbucket.org/Seraphli/matlab-ivc/raw/0bd240dcc5dbeb16ae21e13e695d78bb8641bf7d/LICENSE).
